
package assignment_ds.fourclient.entities;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the assignment_ds.fourclient.entities package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: assignment_ds.fourclient.entities
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PackageIdRequest }
     * 
     */
    public PackageIdRequest createPackageIdRequest() {
        return new PackageIdRequest();
    }

    /**
     * Create an instance of {@link ResponsePackage }
     * 
     */
    public ResponsePackage createResponsePackage() {
        return new ResponsePackage();
    }

    /**
     * Create an instance of {@link UserType }
     * 
     */
    public UserType createUserType() {
        return new UserType();
    }

    /**
     * Create an instance of {@link TrackPackageRequest }
     * 
     */
    public TrackPackageRequest createTrackPackageRequest() {
        return new TrackPackageRequest();
    }

    /**
     * Create an instance of {@link UpdatePackageStatus }
     * 
     */
    public UpdatePackageStatus createUpdatePackageStatus() {
        return new UpdatePackageStatus();
    }

    /**
     * Create an instance of {@link BooleanResponse }
     * 
     */
    public BooleanResponse createBooleanResponse() {
        return new BooleanResponse();
    }

    /**
     * Create an instance of {@link RequestPackage }
     * 
     */
    public RequestPackage createRequestPackage() {
        return new RequestPackage();
    }

}
