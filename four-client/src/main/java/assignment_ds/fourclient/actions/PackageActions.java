package assignment_ds.fourclient.actions;

import assignment_ds.fourclient.Marshaller;
import assignment_ds.fourclient.clientEntities.*;
import assignment_ds.fourclient.entities.*;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import org.w3c.dom.*;
import org.w3c.dom.Element;
import org.w3c.dom.Node;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import java.io.*;
import java.lang.Package;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;


public class PackageActions extends WebServiceGatewaySupport {

    public ResponsePackage addPackage(RequestPackage requestPackage){
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setPackagesToScan("entities");
        marshaller.setClassesToBeBound(RequestPackage.class, ResponsePackage.class);
        getWebServiceTemplate().setMarshaller(marshaller);
        getWebServiceTemplate().setUnmarshaller(marshaller);
        ResponsePackage response = (ResponsePackage) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8080/admin/ws/packages", requestPackage,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/RequestPackage"));

        return response;

    }

    public BooleanResponse removePackage(PackageIdRequest packageIdRequest){
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setPackagesToScan("entities");
        marshaller.setClassesToBeBound(PackageIdRequest.class, BooleanResponse.class);
        getWebServiceTemplate().setMarshaller(marshaller);
        getWebServiceTemplate().setUnmarshaller(marshaller);
        BooleanResponse response = (BooleanResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8080/admin/ws/packages", packageIdRequest,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/PackageIdRequest"));

        return response;

    }

    public BooleanResponse registerTracking(TrackPackageRequest packageIdRequest){
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setPackagesToScan("entities");
        marshaller.setClassesToBeBound(TrackPackageRequest.class, BooleanResponse.class);
        getWebServiceTemplate().setMarshaller(marshaller);
        getWebServiceTemplate().setUnmarshaller(marshaller);
        BooleanResponse response = (BooleanResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8080/admin/ws/packages", packageIdRequest,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/TrackPackageRequest"));

        return response;
    }

    public BooleanResponse updateStatus(UpdatePackageStatus updatePackageStatus){
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setPackagesToScan("entities");
        marshaller.setClassesToBeBound(UpdatePackageStatus.class, BooleanResponse.class);
        getWebServiceTemplate().setMarshaller(marshaller);
        getWebServiceTemplate().setUnmarshaller(marshaller);
        BooleanResponse response = (BooleanResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8080/admin/ws/packages", updatePackageStatus,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/UpdatePackageStatus"));

        return response;
    }

    public ViewAllResponse viewAll(ViewAllRequest viewAllRequest) {
        ViewAllResponse response = new ViewAllResponse();
        try{
            String urlString = "http://localhost:60656/SercivePackages.asmx";
            Marshaller marshaller = new Marshaller();
            String viewAllStr  = marshaller.marshall(viewAllRequest);

            URL urlForInfWebSvc = new URL(urlString);
            URLConnection UrlConnInfWebSvc = urlForInfWebSvc.openConnection();
            HttpURLConnection httpUrlConnInfWebSvc = (HttpURLConnection) UrlConnInfWebSvc;
            httpUrlConnInfWebSvc.setDoOutput(true);
            httpUrlConnInfWebSvc.setDoInput(true);
            httpUrlConnInfWebSvc.setAllowUserInteraction(true);
            httpUrlConnInfWebSvc.setRequestMethod("POST");
            httpUrlConnInfWebSvc.setRequestProperty("Host", "localhost");
            httpUrlConnInfWebSvc.setRequestProperty("Content-Type","application/soap+xml; charset=utf-8");
            httpUrlConnInfWebSvc.setRequestProperty("Content-Length", Integer.toString(viewAllStr.length()));

            OutputStreamWriter infWebSvcReqWriter = new OutputStreamWriter(httpUrlConnInfWebSvc.getOutputStream());

            infWebSvcReqWriter.write(viewAllStr);
            infWebSvcReqWriter.flush();

            BufferedReader infWebSvcReplyReader = new BufferedReader(new InputStreamReader(httpUrlConnInfWebSvc.getInputStream()));
            String line;
            String infWebSvcReplyString = "";
            while ((line = infWebSvcReplyReader.readLine()) != null) {
                infWebSvcReplyString = infWebSvcReplyString.concat(line);
            }


            DocumentBuilderFactory factory =
                    DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            StringBuilder xmlStringBuilder = new StringBuilder();

            xmlStringBuilder.append(infWebSvcReplyString);
            ByteArrayInputStream input = new ByteArrayInputStream(
                    xmlStringBuilder.toString().getBytes("UTF-8"));
            Document doc = builder.parse(input);

            NodeList packages = doc.getElementsByTagName("packagesList");

            for (int temp = 0; temp < packages.getLength(); temp++) {
                Node nNode = packages.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element) nNode;
                    assignment_ds.fourclient.clientEntities.Package p = new assignment_ds.fourclient.clientEntities.Package();

                    p.setId(Long.parseLong(eElement.getElementsByTagName("id").item(0).getTextContent()));

                    Element senderEl = (Element) eElement.getElementsByTagName("sender").item(0);
                    User sender = new User();
                    sender.setId(Long.parseLong(senderEl.getElementsByTagName("id").item(0).getTextContent()));
                    sender.setRole(Roles.CLIENT);
                    p.setSender(sender);

                    Element receiverEl = (Element) eElement.getElementsByTagName("receiver").item(0);
                    User receiver = new User();
                    receiver.setId(Long.parseLong(receiverEl.getElementsByTagName("id").item(0).getTextContent()));
                    receiver.setRole(Roles.CLIENT);
                    p.setReceiver(receiver);

                    p.setName(eElement.getElementsByTagName("name").item(0).getTextContent());

                    p.setDestinationCity(eElement.getElementsByTagName("destinationCity").item(0).getTextContent());

                    p.setSenderCity(eElement.getElementsByTagName("senderCity").item(0).getTextContent());

                    p.setDescription(eElement.getElementsByTagName("description").item(0).getTextContent());

                    p.setTracking(Boolean.parseBoolean(eElement.getElementsByTagName("tracking").item(0).getTextContent()));

                    response.getPackagesList().add(p);
                }

            }

        }catch (Exception e){

        }

        return response;
    }

    public ViewOneResponse viewOne(ViewOneRequest viewOneRequest, Roles r, long clientId) {

        ViewOneResponse response = new ViewOneResponse();
        try{
            String urlString = "http://localhost:60656/SercivePackages.asmx";
            Marshaller marshaller = new Marshaller();
            String viewOneStr = marshaller.marshall(viewOneRequest);

            URL urlForInfWebSvc = new URL(urlString);
            URLConnection UrlConnInfWebSvc = urlForInfWebSvc.openConnection();
            HttpURLConnection httpUrlConnInfWebSvc = (HttpURLConnection) UrlConnInfWebSvc;
            httpUrlConnInfWebSvc.setDoOutput(true);
            httpUrlConnInfWebSvc.setDoInput(true);
            httpUrlConnInfWebSvc.setAllowUserInteraction(true);
            httpUrlConnInfWebSvc.setRequestMethod("POST");
            httpUrlConnInfWebSvc.setRequestProperty("Host", "localhost");
            httpUrlConnInfWebSvc.setRequestProperty("Content-Type","application/soap+xml; charset=utf-8");
            httpUrlConnInfWebSvc.setRequestProperty("Content-Length", Integer.toString(viewOneStr.length()));

            OutputStreamWriter infWebSvcReqWriter = new OutputStreamWriter(httpUrlConnInfWebSvc.getOutputStream());

            infWebSvcReqWriter.write(viewOneStr);
            infWebSvcReqWriter.flush();

            BufferedReader infWebSvcReplyReader = new BufferedReader(new InputStreamReader(httpUrlConnInfWebSvc.getInputStream()));
            String line;
            String infWebSvcReplyString = "";
            while ((line = infWebSvcReplyReader.readLine()) != null) {
                infWebSvcReplyString = infWebSvcReplyString.concat(line);
            }


            DocumentBuilderFactory factory =
                    DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            StringBuilder xmlStringBuilder = new StringBuilder();

            xmlStringBuilder.append(infWebSvcReplyString);
            ByteArrayInputStream input = new ByteArrayInputStream(
                    xmlStringBuilder.toString().getBytes("UTF-8"));
            Document doc = builder.parse(input);

            NodeList packages = doc.getElementsByTagName("package");

            for (int temp = 0; temp < packages.getLength(); temp++) {
                Node nNode = packages.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element) nNode;
                    assignment_ds.fourclient.clientEntities.Package p = new assignment_ds.fourclient.clientEntities.Package();

                    Element senderEl = (Element) eElement.getElementsByTagName("sender").item(0);
                    Element receiverEl = (Element) eElement.getElementsByTagName("receiver").item(0);
                    long senderId = Long.parseLong(senderEl.getElementsByTagName("id").item(0).getTextContent());
                    long receiverId = Long.parseLong(receiverEl.getElementsByTagName("id").item(0).getTextContent());

                    if(senderId == clientId || receiverId == clientId){
                        p.setId(Long.parseLong(eElement.getElementsByTagName("id").item(0).getTextContent()));


                        User sender = new User();
                        sender.setId(senderId);
                        sender.setRole(r);
                        p.setSender(sender);

                        User receiver = new User();
                        receiver.setId(receiverId);
                        receiver.setRole(Roles.CLIENT);
                        p.setReceiver(receiver);

                        p.setName(eElement.getElementsByTagName("name").item(0).getTextContent());

                        p.setDestinationCity(eElement.getElementsByTagName("destinationCity").item(0).getTextContent());

                        p.setSenderCity(eElement.getElementsByTagName("senderCity").item(0).getTextContent());

                        p.setDescription(eElement.getElementsByTagName("description").item(0).getTextContent());

                        p.setTracking(Boolean.parseBoolean(eElement.getElementsByTagName("tracking").item(0).getTextContent()));

                        response.setPackage(p);
                    }

                }

            }

        } catch (Exception e){

        }

        return response;
    }

    public CheckStatusResponse checkStatus(CheckStatusRequest checkStatusRequest) {
        CheckStatusResponse response = new CheckStatusResponse();
        try{
            String urlString = "http://localhost:60656/SercivePackages.asmx";
            Marshaller marshaller = new Marshaller();
            String checkStatusStr = marshaller.marshall(checkStatusRequest);

            URL urlForInfWebSvc = new URL(urlString);
            URLConnection UrlConnInfWebSvc = urlForInfWebSvc.openConnection();
            HttpURLConnection httpUrlConnInfWebSvc = (HttpURLConnection) UrlConnInfWebSvc;
            httpUrlConnInfWebSvc.setDoOutput(true);
            httpUrlConnInfWebSvc.setDoInput(true);
            httpUrlConnInfWebSvc.setAllowUserInteraction(true);
            httpUrlConnInfWebSvc.setRequestMethod("POST");
            httpUrlConnInfWebSvc.setRequestProperty("Host", "localhost");
            httpUrlConnInfWebSvc.setRequestProperty("Content-Type","application/soap+xml; charset=utf-8");
            httpUrlConnInfWebSvc.setRequestProperty("Content-Length", Integer.toString(checkStatusStr.length()));

            OutputStreamWriter infWebSvcReqWriter = new OutputStreamWriter(httpUrlConnInfWebSvc.getOutputStream());

            infWebSvcReqWriter.write(checkStatusStr);
            infWebSvcReqWriter.flush();

            BufferedReader infWebSvcReplyReader = new BufferedReader(new InputStreamReader(httpUrlConnInfWebSvc.getInputStream()));
            String line;
            String infWebSvcReplyString = "";
            while ((line = infWebSvcReplyReader.readLine()) != null) {
                infWebSvcReplyString = infWebSvcReplyString.concat(line);
            }


            DocumentBuilderFactory factory =
                    DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            StringBuilder xmlStringBuilder = new StringBuilder();

            xmlStringBuilder.append(infWebSvcReplyString);
            ByteArrayInputStream input = new ByteArrayInputStream(
                    xmlStringBuilder.toString().getBytes("UTF-8"));
            Document doc = builder.parse(input);

            NodeList statuses = doc.getElementsByTagName("packageStatus");

            for (int temp = 0; temp < statuses.getLength(); temp++) {
                Node nNode = statuses.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element) nNode;
                    Status status = new Status();
                    status.setCity(eElement.getElementsByTagName("city").item(0).getTextContent());
                    status.setTime(eElement.getElementsByTagName("time").item(0).getTextContent());
                    response.getPackageStatus().add(status);
                }

            }


        } catch (Exception e){

        }


        return response;
    }

    public void register(LoginRequest loginRequest){
        try{
            String urlString = "http://localhost:60656/SercivePackages.asmx";
            Marshaller marshaller = new Marshaller();
            String registerRequest = marshaller.register(loginRequest);

            URL urlForInfWebSvc = new URL(urlString);
            URLConnection UrlConnInfWebSvc = urlForInfWebSvc.openConnection();
            HttpURLConnection httpUrlConnInfWebSvc = (HttpURLConnection) UrlConnInfWebSvc;
            httpUrlConnInfWebSvc.setDoOutput(true);
            httpUrlConnInfWebSvc.setDoInput(true);
            httpUrlConnInfWebSvc.setAllowUserInteraction(true);
            httpUrlConnInfWebSvc.setRequestMethod("POST");
            httpUrlConnInfWebSvc.setRequestProperty("Host", "localhost");
            httpUrlConnInfWebSvc.setRequestProperty("Content-Type","application/soap+xml; charset=utf-8");
            httpUrlConnInfWebSvc.setRequestProperty("Content-Length", Integer.toString(registerRequest.length()));

            OutputStreamWriter infWebSvcReqWriter = new OutputStreamWriter(httpUrlConnInfWebSvc.getOutputStream());

            infWebSvcReqWriter.write(registerRequest);
            infWebSvcReqWriter.flush();

            infWebSvcReqWriter.close();
            httpUrlConnInfWebSvc.disconnect();

        } catch (Exception e){

        }
    }

    public LoginResponse login(LoginRequest loginRequest) {

        LoginResponse response=new LoginResponse();

        try{

            String urlString = "http://localhost:60656/SercivePackages.asmx";

            Marshaller marshaller = new Marshaller();
            String loginRequestStr = marshaller.marshall(loginRequest);
            URL urlForInfWebSvc = new URL(urlString);
            URLConnection UrlConnInfWebSvc = urlForInfWebSvc.openConnection();
            HttpURLConnection httpUrlConnInfWebSvc = (HttpURLConnection) UrlConnInfWebSvc;
            httpUrlConnInfWebSvc.setDoOutput(true);
            httpUrlConnInfWebSvc.setDoInput(true);
            httpUrlConnInfWebSvc.setAllowUserInteraction(true);
            httpUrlConnInfWebSvc.setRequestMethod("POST");
            httpUrlConnInfWebSvc.setRequestProperty("Host", "localhost");
            httpUrlConnInfWebSvc.setRequestProperty("Content-Type","application/soap+xml; charset=utf-8");
            httpUrlConnInfWebSvc.setRequestProperty("Content-Length", Integer.toString(loginRequestStr.length()));

            OutputStreamWriter infWebSvcReqWriter = new OutputStreamWriter(httpUrlConnInfWebSvc.getOutputStream());


            infWebSvcReqWriter.write(loginRequestStr);
            infWebSvcReqWriter.flush();

            BufferedReader infWebSvcReplyReader = new BufferedReader(new InputStreamReader(httpUrlConnInfWebSvc.getInputStream()));
            String line;
            String infWebSvcReplyString = "";
            while ((line = infWebSvcReplyReader.readLine()) != null) {
                infWebSvcReplyString = infWebSvcReplyString.concat(line);
            }


            DocumentBuilderFactory factory =
                    DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            StringBuilder xmlStringBuilder = new StringBuilder();

            xmlStringBuilder.append(infWebSvcReplyString);
            ByteArrayInputStream input = new ByteArrayInputStream(
                    xmlStringBuilder.toString().getBytes("UTF-8"));
            Document doc = builder.parse(input);

            NodeList ids = doc.getElementsByTagName("user");

            String id=null;

            Roles r=Roles.CLIENT;
            for (int temp = 0; temp < ids.getLength(); temp++) {
                Node nNode = ids.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element) nNode;
                    id=eElement.getElementsByTagName("id").item(0).getTextContent();
                    String rol=eElement.getElementsByTagName("role").item(0).getTextContent();
                    if(rol.equals("ADMIN")){
                        r=Roles.ADMIN;
                    }
                }

            }

            User user = new User();
            user.setId(Long.parseLong(id));
            user.setRole(r);
            response.setUser(user);

            infWebSvcReqWriter.close();
            infWebSvcReplyReader.close();
            httpUrlConnInfWebSvc.disconnect();
        } catch(Exception e){

        }


        return response;
    }


}
