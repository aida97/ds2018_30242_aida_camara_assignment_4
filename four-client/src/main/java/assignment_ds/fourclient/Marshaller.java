package assignment_ds.fourclient;


import assignment_ds.fourclient.clientEntities.CheckStatusRequest;
import assignment_ds.fourclient.clientEntities.LoginRequest;
import assignment_ds.fourclient.clientEntities.ViewAllRequest;
import assignment_ds.fourclient.clientEntities.ViewOneRequest;


public class Marshaller {
    public String marshall(LoginRequest loginRequest){
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">\n" +
                "  <soap12:Body>\n" +
                "    <login xmlns=\"http://spring.io/guides/gs-producing-web-service/client\">\n" +
                "      <l>\n" +
                "        <username>"+loginRequest.getUsername()+"</username>\n" +
                "        <password>"+loginRequest.getPassword()+"</password>\n" +
                "      </l>\n" +
                "    </login>\n" +
                "  </soap12:Body>\n" +
                "</soap12:Envelope>";

    }

    public String marshall(CheckStatusRequest checkStatusRequest){
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">\n" +
                "  <soap12:Body>\n" +
                "    <checkStatus xmlns=\"http://spring.io/guides/gs-producing-web-service/client\">\n" +
                "      <req>\n" +
                "        <packageId>"+checkStatusRequest.getPackageId()+"</packageId>\n"+
                "        <userId>"+checkStatusRequest.getUserId()+"</userId>\n"+
                "      </req>\n" +
                "    </checkStatus>\n" +
                "  </soap12:Body>\n" +
                "</soap12:Envelope>";
    }

    public String marshall(ViewOneRequest viewOneRequest){
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">\n" +
                "  <soap12:Body>\n" +
                "    <viewOne xmlns=\"http://spring.io/guides/gs-producing-web-service/client\">\n" +
                "      <req>\n" +
                "        <packageId>"+viewOneRequest.getPackageId()+"</packageId>\n" +
                "      </req>\n" +
                "    </viewOne>\n" +
                "  </soap12:Body>\n" +
                "</soap12:Envelope>";
    }

    public String marshall(ViewAllRequest viewAllRequest){
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">\n" +
                "  <soap12:Body>\n" +
                "    <viewAll xmlns=\"http://spring.io/guides/gs-producing-web-service/client\">\n" +
                "      <req>\n" +
                "        <userId>"+viewAllRequest.getUserId()+"</userId>\n" +
                "      </req>\n" +
                "    </viewAll>\n" +
                "  </soap12:Body>\n" +
                "</soap12:Envelope>";
    }

    public String register(LoginRequest loginRequest){
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "  <soap:Body>\n" +
                "    <register xmlns=\"http://spring.io/guides/gs-producing-web-service/client\">\n" +
                "      <req>\n" +
                "        <username>"+loginRequest.getUsername()+"</username>\n" +
                "        <password>"+loginRequest.getPassword()+"</password>\n" +
                "      </req>\n" +
                "    </register>\n" +
                "  </soap:Body>\n" +
                "</soap:Envelope>";
    }
}
