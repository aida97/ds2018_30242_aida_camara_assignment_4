package assignment_ds.fourclient.servlets;

import assignment_ds.fourclient.actions.PackageActions;
import assignment_ds.fourclient.clientEntities.Package;
import assignment_ds.fourclient.clientEntities.User;
import assignment_ds.fourclient.clientEntities.ViewOneRequest;
import assignment_ds.fourclient.clientEntities.ViewOneResponse;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "ViewOneServlet")
public class ViewOneServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ViewOneRequest viewOneRequest = new ViewOneRequest();
        viewOneRequest.setPackageId(Long.parseLong(request.getParameter("packageId")));

        PackageActions actions = new PackageActions();
        HttpSession session = request.getSession(false);
        User user = (User)session.getAttribute("user");

        ViewOneResponse viewOneResponse = actions.viewOne(viewOneRequest, user.getRole(), user.getId());

        String table = "<table><th>ID</th><th>ID sender</th><th>ID Receiver</th><th>name</th><th>description</th>" +
                "<th>sender city</th><th>receiver city</th>";

        Package pack = viewOneResponse.getPackage();

        table = table.concat("<tr>");

        table = table.concat("<td>");
        table = table.concat(Long.toString(pack.getId()));
        table = table.concat("</td>");

        table = table.concat("<td>");
        table = table.concat(Long.toString(pack.getSender().getId()));
        table = table.concat("</td>");

        table = table.concat("<td>");
        table = table.concat(Long.toString(pack.getReceiver().getId()));
        table = table.concat("</td>");

        table = table.concat("<td>");
        table = table.concat(pack.getName());
        table = table.concat("</td>");

        table = table.concat("<td>");
        table = table.concat(pack.getDescription());
        table = table.concat("</td>");

        table = table.concat("<td>");
        table = table.concat(pack.getSenderCity());
        table = table.concat("</td>");

        table = table.concat("<td>");
        table = table.concat(pack.getDestinationCity());
        table = table.concat("</td>");

        table = table.concat("</td>");
        table = table.concat("</table>");

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
        String htmlPage = docType+"<html>\n" +
                "<head><title>" + "Client" + "</title></head>\n" +
                "<body bgcolor = \"#f0f0f0\">\n" +
                "<h1" + "All packages" + "</h1>\n"+
                table +"</body></html>";
        out.println(htmlPage);
    }
}
