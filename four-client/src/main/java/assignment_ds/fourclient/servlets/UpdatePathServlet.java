package assignment_ds.fourclient.servlets;

import assignment_ds.fourclient.actions.PackageActions;
import assignment_ds.fourclient.entities.BooleanResponse;
import assignment_ds.fourclient.entities.UpdatePackageStatus;
import com.sun.org.apache.bcel.internal.generic.BREAKPOINT;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Aida on 12/29/2018.
 */
@WebServlet(name = "UpdatePathServlet")
public class UpdatePathServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        UpdatePackageStatus updatePackageStatus = new UpdatePackageStatus();
        updatePackageStatus.setPackageId(Long.parseLong(request.getParameter("packageId")));
        updatePackageStatus.setCity(request.getParameter("cityUpdate"));
        updatePackageStatus.setTime(request.getParameter("timeUpdate"));

        PackageActions actions = new PackageActions();
        BooleanResponse booleanResponse = actions.updateStatus(updatePackageStatus);

        if(booleanResponse.isBooleanResponse()){
            PrintWriter out = response.getWriter();
            String docType =
                    "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
            String htmlPage = docType+"<html>\n" +
                    "<head><title>" + "Administrator" + "</title></head>\n" +
                    "<h1>Updated path for package</h1>\n</body></html>";
            out.println(htmlPage);
        } else{
            PrintWriter out = response.getWriter();
            String docType =
                    "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
            String htmlPage = docType+"<html>\n" +
                    "<head><title>" + "Administrator" + "</title></head>\n" +
                    "<h1>Some error occurred</h1>\n</body></html>";
            out.println(htmlPage);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
