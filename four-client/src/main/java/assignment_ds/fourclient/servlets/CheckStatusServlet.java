package assignment_ds.fourclient.servlets;

import assignment_ds.fourclient.actions.PackageActions;
import assignment_ds.fourclient.clientEntities.CheckStatusRequest;
import assignment_ds.fourclient.clientEntities.CheckStatusResponse;
import assignment_ds.fourclient.clientEntities.Status;
import assignment_ds.fourclient.clientEntities.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by Aida on 12/29/2018.
 */
@WebServlet(name = "CheckStatusServlet")
public class CheckStatusServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        CheckStatusRequest checkStatusRequest = new CheckStatusRequest();
        checkStatusRequest.setPackageId(Long.parseLong(request.getParameter("packageId")));

        PackageActions actions = new PackageActions();
        HttpSession session = request.getSession(false);
        User user = (User)session.getAttribute("user");
        checkStatusRequest.setUserId(user.getId());

        CheckStatusResponse checkStatusResponse = actions.checkStatus(checkStatusRequest);

        List<Status> statusList = checkStatusResponse.getPackageStatus();

        String table = "<table><th>city</th><th>time</th>";

        for(Status status:statusList){
            table = table.concat("<tr>");

            table = table.concat("<td>");
            table = table.concat(status.getCity());
            table = table.concat("</td>");

            table = table.concat("<td>");
            table = table.concat(status.getTime());
            table = table.concat("</td>");

            table = table.concat("</tr>");
        }

        table = table.concat("</table>");

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
        String htmlPage = docType+"<html>\n" +
                "<head><title>" + "Client" + "</title></head>\n" +
                "<body bgcolor = \"#f0f0f0\">\n" +
                "<h1" + "Package Status" + "</h1>\n"+
                table +"</body></html>";
        out.println(htmlPage);
    }
}
