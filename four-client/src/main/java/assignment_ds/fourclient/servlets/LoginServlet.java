package assignment_ds.fourclient.servlets;

import assignment_ds.fourclient.actions.PackageActions;
import assignment_ds.fourclient.clientEntities.LoginRequest;
import assignment_ds.fourclient.clientEntities.LoginResponse;
import assignment_ds.fourclient.clientEntities.Roles;
import assignment_ds.fourclient.clientEntities.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername(request.getParameter("username"));
        loginRequest.setPassword(request.getParameter("password"));

        PackageActions actions = new PackageActions();
        actions.register(loginRequest);
        String url = request.getContextPath() + "/login.html";
        response.sendRedirect(url);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername(request.getParameter("username"));
        loginRequest.setPassword(request.getParameter("password"));

        PackageActions actions = new PackageActions();
        LoginResponse loginResponse = actions.login(loginRequest);
        HttpSession session = request.getSession(true);
        User user = loginResponse.getUser();
        session.setAttribute("user", user);

        if(user == null) {
            String url = request.getContextPath() + "/login.html";
            response.sendRedirect(url);
        } else if (user.getRole() == Roles.ADMIN) {
            String url = request.getContextPath() + "/admin.html";
            response.sendRedirect(url);
        } else {
            String url = request.getContextPath() + "/client.html";
            response.sendRedirect(url);
        }
    }
}
