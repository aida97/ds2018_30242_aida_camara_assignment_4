package assignment_ds.fourclient.servlets;

import assignment_ds.fourclient.actions.PackageActions;
import assignment_ds.fourclient.entities.RequestPackage;
import assignment_ds.fourclient.entities.ResponsePackage;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "AddPackageServlet")
public class AddPackageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RequestPackage requestPackage=new RequestPackage();
        requestPackage.setSenderId(Long.parseLong(request.getParameter("senderId")));
        requestPackage.setReceiverId(Long.parseLong(request.getParameter("receiverId")));
        requestPackage.setName(request.getParameter("name"));
        requestPackage.setDescription(request.getParameter("description"));
        requestPackage.setSenderCity(request.getParameter("senderCity"));
        requestPackage.setDestinationCity(request.getParameter("receiverCity"));

        PackageActions actions = new PackageActions();
        ResponsePackage responsePackage = actions.addPackage(requestPackage);

        if(responsePackage!=null){
            PrintWriter out = response.getWriter();
            String docType =
                    "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
            String htmlPage = docType+"<html>\n" +
                    "<head><title>" + "Administrator" + "</title></head>\n" +
                    "<h1>Added package</h1>\n" +
                    "generated id: " + responsePackage.getId() +
                    "</body></html>";
            out.println(htmlPage);
        } else {
            PrintWriter out = response.getWriter();
            String docType =
                    "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
            String htmlPage = docType+"<html>\n" +
                    "<head><title>" + "Administrator" + "</title></head>\n" +
                    "<h1>Some error occurred</h1>\n</body></html>";
            out.println(htmlPage);
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
