
package assignment_ds.fourclient.clientEntities;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="packagesList" type="{http://spring.io/guides/gs-producing-web-service/client}Package" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "packagesList"
})
@XmlRootElement(name = "ViewAllResponse", namespace = "http://spring.io/guides/gs-producing-web-service/client")
public class ViewAllResponse {

    @XmlElement(namespace = "http://spring.io/guides/gs-producing-web-service/client")
    protected List<Package> packagesList;

    /**
     * Gets the value of the packagesList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the packagesList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPackagesList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Package }
     * 
     * 
     */
    public List<Package> getPackagesList() {
        if (packagesList == null) {
            packagesList = new ArrayList<Package>();
        }
        return this.packagesList;
    }

}
