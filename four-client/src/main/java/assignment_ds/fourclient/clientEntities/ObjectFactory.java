
package assignment_ds.fourclient.clientEntities;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the assignment_ds.fourclient.clientEntities package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: assignment_ds.fourclient.clientEntities
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link ViewOneResponse }
     * 
     */
    public ViewOneResponse createViewOneResponse() {
        return new ViewOneResponse();
    }

    /**
     * Create an instance of {@link Package }
     * 
     */
    public Package createPackage() {
        return new Package();
    }

    /**
     * Create an instance of {@link ViewOneRequest }
     * 
     */
    public ViewOneRequest createViewOneRequest() {
        return new ViewOneRequest();
    }

    /**
     * Create an instance of {@link LoginRequest }
     * 
     */
    public LoginRequest createLoginRequest() {
        return new LoginRequest();
    }

    /**
     * Create an instance of {@link ViewAllRequest }
     * 
     */
    public ViewAllRequest createViewAllRequest() {
        return new ViewAllRequest();
    }

    /**
     * Create an instance of {@link ViewAllResponse }
     * 
     */
    public ViewAllResponse createViewAllResponse() {
        return new ViewAllResponse();
    }

    /**
     * Create an instance of {@link CheckStatusRequest }
     * 
     */
    public CheckStatusRequest createCheckStatusRequest() {
        return new CheckStatusRequest();
    }

    /**
     * Create an instance of {@link CheckStatusResponse }
     * 
     */
    public CheckStatusResponse createCheckStatusResponse() {
        return new CheckStatusResponse();
    }

    /**
     * Create an instance of {@link Status }
     * 
     */
    public Status createStatus() {
        return new Status();
    }

}
