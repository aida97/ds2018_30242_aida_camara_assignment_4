
package assignment_ds.fourclient.clientEntities;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Roles.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Roles">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CLIENT"/>
 *     &lt;enumeration value="ADMIN"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Roles", namespace = "http://spring.io/guides/gs-producing-web-service/client")
@XmlEnum
public enum Roles {

    CLIENT,
    ADMIN;

    public String value() {
        return name();
    }

    public static Roles fromValue(String v) {
        return valueOf(v);
    }

}
