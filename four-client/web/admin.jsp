<%--
  Created by IntelliJ IDEA.
  User: Aida
  Date: 12/27/2018
  Time: 5:48 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Packages</title>
</head>
<body>

<h1>Add package</h1>
<form action="/saveresult" method="POST">
    <p>Sender ID: <input type="text" name="senderId"></p>
    <p>Receiver ID: <input type="text" name="receiverId"></p>
    <p>Name: <input type="text" name="name"></p>
    <p>Description: <input type="text" name="description"></p>
    <p>Sender City: <input type="text" name="senderCity"></p>
    <p>Receiver City: <input type="text" name="receiverCity"></p>
    <p><input type="submit" value="Add package"></p>
</form>

<h1>Remove package</h1>
<form action="/removeresult" method="POST">
    <p>Package ID: <input type="text" name="packageId"></p>
    <p><input type="submit" value="Remove package"></p>
</form>

<h1>Register package for tracking</h1>
<form action="/registerresult" method="post">
    <p>Package ID: <input type="text" name="packageId"></p>
    <p><input type="submit" value="Register package for tracking"></p>
</form>

<h1>Update status</h1>
<form action="/updateresult" method="post">
    <p>Package ID: <input type="text" name="packageId"></p>
    <p>City: <input type="text" name="cityUpdate"></p>
    <p>Time: <input type="time" name="timeUpdate"></p>
    <p><input type="submit" value="Update package status"></p>
</form>
</body>
</html>
