﻿using System;

using System.Web;
using System.Web.Services;
using System.Data;

using System.Data.SqlClient;

using System.Xml;
using System.IO;
using System.Text;

using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;



namespace assignment_four
{
    /// <summary>
    /// Summary description for SercivePackages
    /// </summary>
    [WebService(Namespace = "http://spring.io/guides/gs-producing-web-service/client")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class SercivePackages : System.Web.Services.WebService
    {
        [WebMethod]
       // [System.Web.Services.Protocols.SoapRpcMethodAttribute("http://spring.io/guides/gs-producing-web-service/client/login", RequestNamespace = "http://spring.io/guides/gs-producing-web-service/client", ResponseNamespace = "http://spring.io/guides/gs-producing-web-service/client")]
        public LoginResponse login(LoginRequest l)
        {
            
            string connStr = "server=localhost;user=root;database=assignment-four;port=3306;password=root";
            MySqlConnection conn = new MySqlConnection(connStr);

          
            
            LoginResponse response = new LoginResponse();
            
            conn.Open();
            string sql = "SELECT id, role FROM user WHERE username=@Username AND password=@Password";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@Username", l.username);
            cmd.Parameters.AddWithValue("@Password", l.password);


            MySqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read())
            {
                response.user = new User();
                response.user.id = Convert.ToInt64(rdr[0]);
                if (((String)rdr[1]).Equals("ADMIN")==true)
                {
                    response.user.role = Roles.ADMIN;
                }
                else
                {
                    response.user.role = Roles.CLIENT;
                }
                
            }

            rdr.Close();
            conn.Close();

           
            return response;
        }

        [WebMethod]
        public void register(LoginRequest req)
        {
            string connStr = "server=localhost;user=root;database=assignment-four;port=3306;password=root";
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = "INSERT INTO user (username, password, role) VALUES (@Username, @Password, CLIENT)";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@Username", req.username);
            cmd.Parameters.AddWithValue("@Password", req.password);

            MySqlDataReader rdr = cmd.ExecuteReader();

            rdr.Close();
            conn.Close();

        }

        [WebMethod]
        public ViewAllResponse viewAll(ViewAllRequest req)
        {
            
            long id = Convert.ToInt64(req.userId);

            string connStr = "server=localhost;user=root;database=assignment-four;port=3306;password=root";
            MySqlConnection conn = new MySqlConnection(connStr);


            ViewAllResponse response = new ViewAllResponse();
            response.packagesList = new Package[50];


            conn.Open();
            string sql = "SELECT * FROM package WHERE idSender=@Id OR idReceiver=@Id";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@Id", id);


            MySqlDataReader rdr = cmd.ExecuteReader();
            int i = 0;
            while (rdr.Read())
            {
                Package p = new Package();
                p.id = Convert.ToInt64(rdr["id"]);
                p.name = (String)rdr["name"];
                p.description = (String)rdr["description"];
                p.destinationCity = (String)rdr["destinationcity"];
                p.senderCity = (String)rdr["sendercity"];
                p.sender = new User();
                p.receiver = new User();
                p.sender.id = Convert.ToInt64(rdr["idsender"]);
                p.receiver.id = Convert.ToInt64(rdr["idreceiver"]);
                response.packagesList.SetValue(p, i);
                i++;
            }

            rdr.Close();
            conn.Close();


            return response;

        }

        [WebMethod]
        public ViewOneResponse viewOne(ViewOneRequest req)
        {
            long id = Convert.ToInt64(req.packageId);

            string connStr = "server=localhost;user=root;database=assignment-four;port=3306;password=root";
            MySqlConnection conn = new MySqlConnection(connStr);

            conn.Open();
            string sql = "SELECT * FROM package WHERE id=@Id";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@Id", id);

            MySqlDataReader rdr = cmd.ExecuteReader();
            ViewOneResponse response = new ViewOneResponse();
            Package p = new Package();
            if (rdr.Read())
            {
                p.id = Convert.ToInt64(rdr["id"]);
                p.name = (String)rdr["name"];
                p.description = (String)rdr["description"];
                p.destinationCity = (String)rdr["destinationcity"];
                p.senderCity = (String)rdr["sendercity"];
                p.sender = new User();
                p.receiver = new User();
                p.sender.id = Convert.ToInt64(rdr["idsender"]);
                p.receiver.id = Convert.ToInt64(rdr["idreceiver"]);
            }

            response.package = p;

            rdr.Close();
            conn.Close();

            return response;
        }

        [WebMethod]
        public CheckStatusResponse checkStatus(CheckStatusRequest req)
        {

            long id = Convert.ToInt64(req.packageId);
            long clientId = Convert.ToInt64(req.userId);

            string connStr = "server=localhost;user=root;database=assignment-four;port=3306;password=root";
            MySqlConnection conn = new MySqlConnection(connStr);

            conn.Open();
            string sql = "SELECT packagepath.city, packagepath.time from packagepath JOIN package ON packagepath.packageId = package.id WHERE packagepath.packageid=@Id AND (package.idsender=@ClientId OR package.idreceiver=@ClientId)";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@Id", id);
            cmd.Parameters.AddWithValue("@ClientId", clientId);

            MySqlDataReader rdr = cmd.ExecuteReader();

            CheckStatusResponse response = new CheckStatusResponse();
            response.packageStatus = new Status[50];
            int i = 0;
            while (rdr.Read())
            {
                Status s = new Status();
                s.city = (string)rdr["city"];
                s.time = (string)rdr["time"];
                response.packageStatus.SetValue(s, i);
                i++;
            }
            return response;
        }
    }
}
