package assignment_ds.endPoints;


import assignment_ds.entities.Package;
import assignment_ds.four.*;
import assignment_ds.servicies.PackageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class PackageEndPoint {
    private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

    private PackageService packageService;


    @Autowired
    public PackageEndPoint(PackageService packageService) {
        this.packageService = packageService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "RequestPackage")
    @ResponsePayload
    public ResponsePackage addPackage(@RequestPayload RequestPackage packageRequest){
        return packageService.addPackage(packageRequest);
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "PackageIdRequest")
    @ResponsePayload
    public BooleanResponse removePackage(@RequestPayload PackageIdRequest packageIdRequest){
        return packageService.removePackage(packageIdRequest);
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "TrackPackageRequest")
    @ResponsePayload
    public BooleanResponse registerTracking(@RequestPayload TrackPackageRequest packageIdRequest){
        return packageService.registerTracking(packageIdRequest);
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "UpdatePackageStatus")
    @ResponsePayload
    public BooleanResponse updatePackage(@RequestPayload UpdatePackageStatus updatePackageStatus){
        return packageService.updatePackage(updatePackageStatus);
    }

}
