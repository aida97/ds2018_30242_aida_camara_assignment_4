package assignment_ds.mappers;


import assignment_ds.entities.Package;
import assignment_ds.entities.Role;
import assignment_ds.entities.User;
import assignment_ds.four.RequestPackage;
import assignment_ds.four.ResponsePackage;
import assignment_ds.four.UserType;

public class Mapper {

    public Package map(RequestPackage requestPackage, User sender, User receiver){
        Package pack = new Package();
        pack.setSender(sender);
        pack.setReceiver(receiver);
        pack.setName(requestPackage.getName());
        pack.setDescription(requestPackage.getDescription());
        pack.setSenderCity(requestPackage.getSenderCity());
        pack.setDestinationCity(requestPackage.getDestinationCity());

        return pack;
    }

    public ResponsePackage map(Package pack){
        ResponsePackage responsePackage = new ResponsePackage();

        responsePackage.setId(pack.getId());
        UserType sender = new UserType();
        sender.setId(pack.getSender().getId());
        if(pack.getSender().getRole() == Role.ADMIN){
            sender.setRole(assignment_ds.four.Role.ADMIN);
        }
        else {
            sender.setRole(assignment_ds.four.Role.CLIENT);
        }
        responsePackage.setSender(sender);

        UserType receiver = new UserType();
        receiver.setId(pack.getReceiver().getId());

        if(pack.getReceiver().getRole() == Role.ADMIN){
            receiver.setRole(assignment_ds.four.Role.ADMIN);
        }
        else{
            receiver.setRole(assignment_ds.four.Role.CLIENT);
        }
        responsePackage.setReceiver(receiver);

        responsePackage.setName(pack.getName());
        responsePackage.setDescription(pack.getDescription());
        responsePackage.setSenderCity(pack.getSenderCity());
        responsePackage.setDestinationCity(pack.getDestinationCity());
        responsePackage.setTracking(pack.isTracked());

        return responsePackage;
    }
}
