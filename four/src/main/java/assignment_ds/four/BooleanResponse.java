
package assignment_ds.four;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="booleanResponse" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "booleanResponse"
})
@XmlRootElement(name = "BooleanResponse", namespace = "http://spring.io/guides/gs-producing-web-service")
public class BooleanResponse {

    @XmlElement(namespace = "http://spring.io/guides/gs-producing-web-service")
    protected boolean booleanResponse;

    /**
     * Gets the value of the booleanResponse property.
     * 
     */
    public boolean isBooleanResponse() {
        return booleanResponse;
    }

    /**
     * Sets the value of the booleanResponse property.
     * 
     */
    public void setBooleanResponse(boolean value) {
        this.booleanResponse = value;
    }

}
