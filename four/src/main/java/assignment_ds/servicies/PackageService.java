package assignment_ds.servicies;


import assignment_ds.entities.Package;
import assignment_ds.entities.PackagePath;
import assignment_ds.entities.User;
import assignment_ds.four.*;
import assignment_ds.mappers.Mapper;
import assignment_ds.repositories.PackageRepository;
import assignment_ds.repositories.PackageStatusRepository;
import assignment_ds.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PackageService {

    private PackageRepository packageRepository;
    private UserRepository userRepository;
    private PackageStatusRepository packageStatusRepository;
    private Mapper mapper;

    @Autowired
    public PackageService(PackageRepository packageRepository, UserRepository userRepository, PackageStatusRepository packageStatusRepository, Mapper mapper) {
        this.packageRepository = packageRepository;
        this.userRepository = userRepository;
        this.packageStatusRepository = packageStatusRepository;
        this.mapper = mapper;
    }

    public ResponsePackage addPackage(RequestPackage requestPackage){
        User sender = userRepository.findById(requestPackage.getSenderId()).orElseThrow(()-> new RuntimeException("User Sender not found"));
        User receiver = userRepository.findById(requestPackage.getReceiverId()).orElseThrow(()-> new RuntimeException("User Receiver not found"));

        Package p = mapper.map(requestPackage, sender,receiver);
        Package s = packageRepository.save(p);
        return mapper.map(s);
    }

    public BooleanResponse removePackage(PackageIdRequest packageIdRequest) {
        Package p = packageRepository.findById(packageIdRequest.getPackageId()).orElseThrow(()->new RuntimeException("Package not found"));

        packageRepository.delete(p);
        BooleanResponse booleanResponse = new BooleanResponse();
        booleanResponse.setBooleanResponse(true);
        return booleanResponse;
    }

    public BooleanResponse registerTracking(TrackPackageRequest packageIdRequest) {

        BooleanResponse booleanResponse = new BooleanResponse();
        try {
            Package p = packageRepository.findById(packageIdRequest.getPackageId()).orElseThrow(()->new RuntimeException("Package not found"));
            p.setTracked(true);
            packageRepository.save(p);
            booleanResponse.setBooleanResponse(true);
        } catch (RuntimeException e) {
            booleanResponse.setBooleanResponse(false);
        }
        return booleanResponse;
    }

    public BooleanResponse updatePackage(UpdatePackageStatus updatePackageStatus) {
        BooleanResponse booleanResponse = new BooleanResponse();
        try {
            Package p = packageRepository.findById(updatePackageStatus.getPackageId()).orElseThrow(()->new RuntimeException("Package not found"));

            if(p.isTracked()){
                PackagePath packagePath = new PackagePath();
                packagePath.setaPackage(p);
                packagePath.setCity(updatePackageStatus.getCity());
                packagePath.setTime(updatePackageStatus.getTime());
                packageStatusRepository.save(packagePath);
                booleanResponse.setBooleanResponse(true);
            } else{
                booleanResponse.setBooleanResponse(false);
            }
        } catch (RuntimeException e) {
            booleanResponse.setBooleanResponse(false);
        }
        return booleanResponse;
    }
}
