package assignment_ds.entities;

import javax.persistence.*;
import java.lang.invoke.LambdaConversionException;
import java.util.HashSet;
import java.util.Set;

@Entity
public class User {
    @Id
    private Long id;

    @Column
    private String username;

    @Column
    private String password;

    @Enumerated(EnumType.STRING)
    @Column
    private Role role;

    @OneToMany(mappedBy = "sender", fetch=FetchType.LAZY)
    private Set<Package> sentPackages = new HashSet<>();

    @OneToMany(mappedBy = "receiver", fetch=FetchType.LAZY)
    private Set<Package> receivedPackages = new HashSet<>();

    public User() {
    }

    public User(String username, String password, Role role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Package> getSentPackages() {
        return sentPackages;
    }

    public void setSentPackages(Set<Package> sentPackages) {
        this.sentPackages = sentPackages;
    }

    public Set<Package> getReceivedPackages() {
        return receivedPackages;
    }

    public void setReceivedPackages(Set<Package> receivedPackages) {
        this.receivedPackages = receivedPackages;
    }
}
