package assignment_ds.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="package")
public class Package {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="idsender")
    private User sender;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="idreceiver")
    private User receiver;

    @Column(name="name")
    private String name;

    @Column(name="description")
    private String description;

    @Column(name="sendercity")
    private String senderCity;

    @Column(name="destinationcity")
    private String destinationCity;

    @Column(name="tracked")
    private boolean tracked=false;

    @OneToMany(mappedBy = "aPackage", fetch=FetchType.LAZY)
    private Set<PackagePath> packagePaths;

    public Package(){

    }

    public Package(long id, User sender, User receiver, String name, String description, String senderCity, String destinationCity, boolean tracked, Set<PackagePath> packagePaths) {
        this.id=id;
        this.sender = sender;
        this.receiver = receiver;
        this.name = name;
        this.description = description;
        this.senderCity = senderCity;
        this.destinationCity = destinationCity;
        this.tracked = tracked;
        this.packagePaths = packagePaths;
    }

    public Set<PackagePath> getPackagePaths() {
        return packagePaths;
    }

    public void setPackagePaths(Set<PackagePath> packagePaths) {
        this.packagePaths = packagePaths;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSenderCity() {
        return senderCity;
    }

    public void setSenderCity(String senderCity) {
        this.senderCity = senderCity;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public boolean isTracked() {
        return tracked;
    }

    public void setTracked(boolean tracked) {
        this.tracked = tracked;
    }
}
