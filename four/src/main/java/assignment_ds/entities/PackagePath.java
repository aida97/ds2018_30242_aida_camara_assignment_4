package assignment_ds.entities;


import javax.persistence.*;

@Entity
@Table(name="packagepath")
public class PackagePath {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="packageid")
    private
    Package aPackage;

    @Column(name="city")
    private
    String city;

    @Column(name="time")
    private
    String time;

    public PackagePath() {
    }

    public PackagePath(Package aPackage, String city, String time) {
        this.aPackage = aPackage;
        this.city = city;
        this.time = time;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Package getaPackage() {
        return aPackage;
    }

    public void setaPackage(Package aPackage) {
        this.aPackage = aPackage;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
