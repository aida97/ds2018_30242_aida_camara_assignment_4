package assignment_ds.repositories;

import assignment_ds.entities.PackagePath;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PackageStatusRepository extends JpaRepository<PackagePath, Long> {
}
